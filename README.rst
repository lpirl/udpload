``udpload``
===========

.. image:: https://gitlab.com/lpirl/udpload/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/udpload/pipelines
  :align: right

``udpload`` is a tool to send UDP packets with certain load profiles,
e.g.::

        bursts, fixed pps             short bursts, flood

      |                              | .~.    .~.    .~.
      |                              | | |    | |    | |
      | .--.  .--.  .--.  .          | | |    | |    | |
  pps | |  |  |  |  |  |  |      pps | | |    | |    | |
      | |  |  |  |  |  |  |          | | |    | |    | |
      |_|  |__|  |__|  |__|          |_| |____| |____| |__
      |––––––––––––––––––––          |––––––––––––––––––––
              time                           time


          increasing pps             bursts, increasing pps

      |               __|            |
      |            __|               |                .-.
      |         __|                  |           .-.  | |
  pps |      __|                 pps |      .-.  | |  | |
      |   __|                        | .-.  | |  | |  | |
      |__|                           |_| |__| |__| |__| |_
      |––––––––––––––––––––          |––––––––––––––––––––
              time                           time


            bursts, increasing pps, with maximum pps

      |                    .-.                 .-.
      |                .-. | |             .-. | |
      |           .-.  | | | |         .-. | | | |
  pps |      .-.  | |  | | | |     .-. | | | | | |     .-.
      | .-.  | |  | |  | | | | .-. | | | | | | | | .-. | |
      |_| |__| |__| |__| |_| |_| |_| |_| |_| |_| |_| |_| |
      |–––––––––––––––––––––––––––––––––––––––––––––––––––
                            time


This can be used, e.g., to benchmark or stress test on OSI layer 4.

Some more properties:

* focus on accuracy
  (packets per second, interval lengths, low interval drift)
* easy to install (pure Python 3, just download)
* easy to extend (pure Python 3, just edit :))

``udpload`` has been developed in the context of the work:

*Behnke, Ilja, Lukas Pirl, Lauritz Thamsen, Robert Danicki, Andreas
Polze, and Odej Kao. "Interrupting Real-Time IoT Tasks: How Bad Can
It Be to Connect Your Critical Embedded System to the Internet?"*

usage
=====

::

  usage: udpload [-h] [-d] [-v] [-f FREQUENCY] [--increment-frequency
                 INCREMENT_FREQUENCY] [-l BURST_LENGTH] [-i
                 BURST_INTERVAL] [-p PAYLOAD] [-y] [--priority
                 PRIORITY] destination_ip destination_port

  This is a simple tool for generating UDP network traffic. It can send
  UPD packets to an IP's port with a variable amount of random payload;
  in variable frequency; in bursts of variable length and a variable
  inter-burst interval.

  positional arguments:
    destination_ip        IP address to send the packets to
    destination_port      port (at IP address) to send the packets to

  optional arguments:
    -h, --help            show this help message and exit
    -d, --debug           turn on debug messages (default: False)
    -v, --verbose         turn on verbose messages (default: False)
    -f FREQUENCY, --frequency FREQUENCY
                          frequency, in Hz, with which packets will be
                          sent; a value of "inf" will make sending go
                          as fast as possible (default: inf)
    --increment-frequency INCREMENT_FREQUENCY
                          frequency, in Hz, by which to increase the
                          packet send frequency each burst; starting
                          from --increment-frequency, ending at
                          -f/--frequency (default: 0)
    -l BURST_LENGTH, --burst-length BURST_LENGTH
                          length of bursts, in ms; the special value of
                          "inf" will cause an infinitive burst length
                          (default: inf)
    -i BURST_INTERVAL, --burst-interval BURST_INTERVAL
                          interval between bursts, in ms (i.e., between
                          start of one burst one and start of the next
                          burst) (default: inf)
    -p PAYLOAD, --payload PAYLOAD
                          amount of random payload, in bytes, packets
                          will be filled with (default: 0)
    -y, --ignore-connection-errors
                          keep going, even if there are errors reaching
                          the destination (default: False)
    --priority PRIORITY   Linux packet priority to use (SO_PRIORITY/SKB
                          priority) (default: 0)

comparison with hping3
======================

Considering UDP only, ``udpload`` is quite similar to `hping3
<https://tools.kali.org/information-gathering/hping3>`__.

However, ``udpload`` has built-in support to send packets in load
profiles. To achieve the same with ``hping3`` surrounding tooling would
be needed (``timeout``, ``sleep``, etc.) which would reduce accuracy.

Interestingly, even though ``updload`` is implemented in an interpreted
scripting language, it achieves even higher packet rates in flood mode
than ``hping3``:

.. list-table::
  :header-rows: 1

  - - command
    - packets per second
  - - ``hping3`` ¹
    - ~ 1.1 M
  - - ``udpload`` ² (cPython3)
    - ~ 2 M
  - - ``udpload`` ² (pypy3)
    - ~ 2.2 M

* ¹ ``timeout -s SIGINT 10 hping3 --flood --udp --destport 42 127.0.0.1``
* ² ``timeout -s SIGINT 10 udpload -yd 127.0.0.1 42``
