smoke_test: CMD_BEGIN ?= timeout -s SIGINT 3 ./udpload -dy
smoke_test: CMD_END ?= 127.0.0.1 42 2>&1
smoke_test:

	$(CMD_BEGIN) $(CMD_END) | grep "frequency: inf"

	$(CMD_BEGIN) -f1 $(CMD_END) | grep "frequency: 1.0"

	$(CMD_BEGIN) -p 1024 $(CMD_END) | grep "generating 1024 "

	# adhere to interval "often and closely enough"
	[ `$(CMD_BEGIN) -l 100 -i 100 $(CMD_END) \
		| grep -c " for 0.1000"` -gt 20 ]

	# adhere to packet frequency "good enough"
	[ `$(CMD_BEGIN) -l 100 -i 100 -f 100 $(CMD_END) | \
		grep -c 'packets (~ '` -gt 10 ]

	# increment and cycle frequencies
	[ `$(CMD_BEGIN) -l 100 -i 100 -f 1000 --increment-frequency 100 \
		$(CMD_END) | grep -c "sending with frequency: 300.0"` -gt 1 ]
